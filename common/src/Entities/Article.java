package Entities;

import java.sql.Timestamp;

/**
 * Created by Zane on 10/1/2015.
 */
public class Article {
    private String title;
    private String body;
    private Timestamp date;
    private String url;
    private String raw_date;
    private String hash;
    private int domain_id;
    private String summary;
    private String loc;
    private String tags;

    public Article(String title, String summary, String body, Timestamp date, String url, String raw_date, String hash, int domain_id, String loc, String tags) {
        this.title = title;
        this.summary = summary;
        this.body = body;
        this.date = date;
        this.url = url;
        this.raw_date = raw_date;
        this.hash = hash;
        this.domain_id = domain_id;
        this.loc = loc;
        this.tags = tags;

    }

    public Article() {
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Article(String title, String summary, String body, String url, String raw_date, String hash, int domain_id, String loc, String tags) {
        this.title = title;
        this.summary = summary;
        this.body = body;
        this.url = url;
        this.date = null;
        this.raw_date = raw_date;
        this.hash = hash;
        this.domain_id = domain_id;
        this.loc = loc;
        this.tags = tags;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public int getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(int domain_id) {
        this.domain_id = domain_id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getRaw_date() {
        return raw_date;
    }

    public void setRaw_date(String raw_date) {
        this.raw_date = raw_date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Article{" +
                "title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", date=" + date +
                ", url='" + url + '\'' +
                ", raw_date='" + raw_date + '\'' +
                ", hash='" + hash + '\'' +
                ", summary='" + summary + '\'' +
                '}';
    }

    public boolean isComplete(){
        //TODO
        return true;
    }

    public boolean isValid(){
        if(title == null || title.equals("")){
            return false;
        }
        if(body == null || body.equals("")){
            return false;
        }
        return true;
    }
}
