package Entities;

/**
 * Created by Zane on 11/6/2015.
 */
public class Organization extends ParsedEntity {

    public Organization(int position, String s) {
        super(position);
        super.setType("ORGANIZATION");
        super.setRawValue(s);
    }
}
