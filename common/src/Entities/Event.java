package Entities;

import Utilities.DateParser;
import Utilities.StringUtilities;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Zane on 11/3/2015.
 */
public class Event {
    private String rawTitle;
    private String rawBody;
    private long date;
    private String summarizedBody;
    private String primaryLocation;
    private ArrayList<String> locations;
    private ArrayList<String> people;
    private ArrayList<String> organizations;
    private long latitude;
    private long longitude;
    private long upvotes;
    private long downvotes;

    public Event() {
        people = new ArrayList<>();
        organizations = new ArrayList<>();
        locations = new ArrayList<>();
    }

    public Event(String rawTitle, String rawBody, long date) {
        this.rawTitle = rawTitle;
        this.rawBody = rawBody;
        this.date = date;
    }

    public Event(Article a) throws ParseException {
        rawTitle = a.getTitle();
        rawBody = a.getBody();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (date != event.date) return false;
        if (!rawTitle.equals(event.rawTitle)) return false;
        return rawBody.equals(event.rawBody);

    }

    @Override
    public int hashCode() {
        int result = rawTitle.hashCode();
        result = 31 * result + rawBody.hashCode();
        result = 31 * result + (int) (date ^ (date >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "summarizedBody='" + summarizedBody + '\'' +
                ", rawBody='" + rawBody + '\'' +
                ", rawTitle='" + rawTitle + '\'' +
                ", date=" + date +
                ", primaryLocation='" + primaryLocation + '\'' +
                ", locations='" + locations.toString() + '\'' +
                ", organizations='" + organizations.toString() + '\'' +
                ", people='" + people.toString() + '\'' +
                '}';
    }

    /**
     * Returns the first 300 characters from the body truncated to the last period.
     * @return String max 300 characters
     */
    public String getRawExcerpt(){
        if(rawBody.isEmpty()){
            return null;
        }
        String excerpt = rawBody.substring(0,300);
        excerpt = excerpt.substring(0, excerpt.lastIndexOf("."));
        return excerpt;
    }
    public List<String> getTags(){
        List<String> combined = new ArrayList<>();
        combined.addAll(people);
        combined.addAll(organizations);
        combined.addAll(locations);
        return combined;
    }

    public double computeEventSimilarity(Event e){
        double overallSimilarity = 0;
        //Create Hashsets for this Events tags
        //TODO normalize character case
        HashSet<String> people = new HashSet<>(this.people);
        HashSet<String> organizations = new HashSet<>(this.organizations);
        HashSet<String> locations = new HashSet<>(this.locations);

        //Keep only intersecting values in original sets
        people.retainAll(new HashSet<String>(e.getPeople()));
        organizations.retainAll(new HashSet<String>(e.getOrganizations()));
        locations.retainAll(new HashSet<String>(e.getLocations()));

        //Calculate each category overlap amount
        double peopleSimilarity = 0;
        if(e.getPeople().size() != 0) {
            peopleSimilarity = (people.size() / e.getPeople().size()) * 100;
        }
        double organizationSimilarity = 0;
        if(e.getOrganizations().size() != 0) {
            organizationSimilarity = (organizations.size() / e.getOrganizations().size()) * 100;
           // System.out.println(organizationSimilarity +", " + organizations.size()+", "+e.getOrganizations().size());
        }
        double locationSimilarity = 0;
        if(e.getLocations().size() != 0) {
            locationSimilarity = (locations.size() / e.getLocations().size()) * 100;
        }

        //Calculate title overlap
        double titleSimilarity = StringUtilities.similarity(this.getRawTitle().toLowerCase(), e.getRawTitle().toLowerCase()) * 100;

        //Compute with weights
        overallSimilarity = (titleSimilarity * .25) + (peopleSimilarity * .25) + (locationSimilarity * .25) + (organizationSimilarity *.25);

        //System.out.println(titleSimilarity + ", " + peopleSimilarity + ", " + locationSimilarity + ", " + organizationSimilarity);
        return overallSimilarity;
    }

    public double computeArticleSimilarity(){
        double overallSimilarity = 0;
        return overallSimilarity;
    }

    public Event(Event e){
        //TODO complete
        this.rawTitle = e.getRawTitle();
        this.rawBody = e.getRawBody();
        this.locations = e.getLocations();
        this.organizations = e.getOrganizations();
        this.people = e.getPeople();
    }
    public String getRawTitle() {
        return rawTitle;
    }

    public void setRawTitle(String rawTitle) {
        this.rawTitle = rawTitle;
    }

    public String getRawBody() {
        return rawBody;
    }

    public void setRawBody(String rawBody) {
        this.rawBody = rawBody;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getSummarizedBody() {
        return summarizedBody;
    }

    public void setSummarizedBody(String summarizedBody) {
        this.summarizedBody = summarizedBody;
    }

    public String getPrimaryLocation() {
        return primaryLocation;
    }

    public void setPrimaryLocation(String primaryLocation) {
        this.primaryLocation = primaryLocation;
    }

    public ArrayList<String> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<String> locations) {
        this.locations = locations;
    }

    public ArrayList<String> getPeople() {
        return people;
    }

    public void setPeople(ArrayList<String> people) {
        this.people = people;
    }

    public ArrayList<String> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(ArrayList<String> organizations) {
        this.organizations = organizations;
    }

    public long getLatitude() {
        return latitude;
    }

    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }

    public long getLongitude() {
        return longitude;
    }

    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }

    public long getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(long upvotes) {
        this.upvotes = upvotes;
    }

    public long getDownvotes() {
        return downvotes;
    }

    public void setDownvotes(long downvotes) {
        this.downvotes = downvotes;
    }
    public void addOrganization(String s){
        this.organizations.add(s.toLowerCase());
    }
    public void addPerson(String s){
        this.people.add(s.toLowerCase());
    }
    public void addLocation(String s){
        this.locations.add(s.toLowerCase());
    }
}
