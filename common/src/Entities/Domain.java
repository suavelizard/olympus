package Entities;

/**
 * Created by Zane on 10/8/2015.
 */
public class Domain {
    private int id;
    private String url;
    private String hash;

    public Domain() {
    }

    public Domain(int id, String url) {
        this.id = id;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
