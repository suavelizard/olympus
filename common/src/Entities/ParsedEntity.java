package Entities;

/**
 * Created by Zane on 11/6/2015.
 */
public abstract class ParsedEntity {
    private String rawValue;
    private String parsedValue;
    public final int position;
    private String type;
    private boolean resolved;
    public ParsedEntity(int position, String rawValue, String parsedValue) {
        this.position = position;
        this.rawValue = rawValue;
        this.parsedValue = parsedValue;
        this.resolved = false;
    }

    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParsedEntity that = (ParsedEntity) o;

        if (!rawValue.equals(that.rawValue)) return false;
        return type.equals(that.type);

    }

    @Override
    public int hashCode() {
        int result = rawValue.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    public ParsedEntity(int position) {
        this.position = position;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public String getParsedValue() {
        return parsedValue;
    }

    public void setParsedValue(String parsedValue) {
        this.parsedValue = parsedValue;
    }

    public int getPosition() {
        return position;
    }
}
