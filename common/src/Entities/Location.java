package Entities;

import com.bericotech.clavin.gazetteer.GeoName;

/**
 * Created by Zane on 11/6/2015.
 */
public class Location extends ParsedEntity{
    private int occurrences = 0;
    private GeoName geoname;
    private boolean fuzzy;
    private float confidence;
    public Location(int position,String s) {
        super(position);
        super.setType("LOCATION");
        super.setRawValue(s);
    }

    public int getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(int occurrences) {
        this.occurrences = occurrences;
    }
    public void incrementOccurences(){
        this.occurrences++;
    }

    public GeoName getGeoname() {
        return geoname;
    }

    public void setGeoname(GeoName geoname) {
        this.geoname = geoname;
    }

    public boolean isFuzzy() {
        return fuzzy;
    }

    public void setFuzzy(boolean fuzzy) {
        this.fuzzy = fuzzy;
    }

    public float getConfidence() {
        return confidence;
    }

    public void setConfidence(float confidence) {
        this.confidence = confidence;
    }
}
