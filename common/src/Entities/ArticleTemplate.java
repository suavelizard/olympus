package Entities;

/**
 * Created by Zane on 9/30/2015.
 */
public class ArticleTemplate {
    private String title_identifier;
    private String body_identifier;
    private String date_identifier;
    private String date_format;
    private int domain_id;

    public int getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(int domain_id) {
        this.domain_id = domain_id;
    }

    public String getTitle_identifier() {
        return title_identifier;
    }

    public void setTitle_identifier(String title_identifier) {
        this.title_identifier = title_identifier;
    }

    public String getBody_identifier() {
        return body_identifier;
    }

    public void setBody_identifier(String body_identifier) {
        this.body_identifier = body_identifier;
    }

    public String getDate_identifier() {
        return date_identifier;
    }

    public void setDate_identifier(String date_identifier) {
        this.date_identifier = date_identifier;
    }

    public String getDate_format() {
        return date_format;
    }

    public void setDate_format(String date_format) {
        this.date_format = date_format;
    }
}
