package Entities;

/**
 * Created by Zane on 11/6/2015.
 */
public class Person extends ParsedEntity {
    public Person(int position, String s) {
        super(position);
        super.setType("PERSON");
        super.setRawValue(s);
    }

}
