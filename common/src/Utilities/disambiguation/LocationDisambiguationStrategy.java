package Utilities.disambiguation;

import Entities.Location;
import Utilities.LocationResolver;
import com.bericotech.clavin.resolver.ResolvedLocation;

import java.util.List;

/**
 * Wrapper around disambiguation strategies, so we can try and compare different ones
 * 
 * @author rahulb
 */
public interface LocationDisambiguationStrategy extends DisambiguationStrategy {

    /**
     * For each candidate list, select the best candidate.
     * 
     * @param allPossibilities
     *            Set of candidate lists to sort through.
     * @return Set of the best candidate choices.
     */
    public abstract List<ResolvedLocation> select(LocationResolver resolver,
            List<List<ResolvedLocation>> allPossibilities);

    public abstract void logStats();
    
}