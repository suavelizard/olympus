package Utilities;

import Entities.Location;
import com.bericotech.clavin.resolver.ResolvedLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zane on 11/8/2015.
 */
public class ListUtlilties {
    public static List<Location> ResolvedLocationsToLocations(List<ResolvedLocation> resolvedLocations){
        List<Location> locations = new ArrayList<Location>();
        for (ResolvedLocation r: resolvedLocations){
            Location l = new Location(r.getLocation().getPosition(),r.getMatchedName());
            l.setGeoname(r.getGeoname());
            locations.add(l);
        }
        return locations;

    }


}
