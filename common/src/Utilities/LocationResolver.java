package Utilities;

import Entities.Location;
import Utilities.disambiguation.HeuristicDisambiguationStrategy;
import Utilities.disambiguation.LocationDisambiguationStrategy;
import com.bericotech.clavin.ClavinException;
import com.bericotech.clavin.gazetteer.GeoName;
import com.bericotech.clavin.gazetteer.query.FuzzyMode;
import com.bericotech.clavin.gazetteer.query.Gazetteer;
import com.bericotech.clavin.gazetteer.query.QueryBuilder;
import com.bericotech.clavin.resolver.ClavinLocationResolver;
import com.bericotech.clavin.resolver.ResolvedLocation;
import exceptions.UnknownGeoNameIdException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Zane on 11/8/2015.
 */
public class LocationResolver {//extends ClavinLocationResolver {
    public static final int MAX_HIT_DEPTH = 10;
    ClavinLocationResolver clr;
    private final Gazetteer gazetteer;
    private static HashSet<String> demonyms;
    private LocationDisambiguationStrategy disambiguationStrategy;

    private boolean filterOutDemonyms = false;

    public LocationResolver(Gazetteer gazetteer) {
        this.gazetteer = gazetteer;
        clr = new ClavinLocationResolver(this.gazetteer);

        //super(gazetteer);
        disambiguationStrategy = new HeuristicDisambiguationStrategy();
    }

    public GeoName getByGeoNameId(int geoNameId) throws UnknownGeoNameIdException {
        try {
          return getGazetteer().getGeoName(geoNameId);
        } catch (ClavinException ce) {
            throw new UnknownGeoNameIdException(geoNameId);
        }
    }
        public Gazetteer getGazetteer() {
            return gazetteer;
        }

    public static boolean isDemonym(Location extractedLocation) {
        if(demonyms == null) {
            demonyms = new HashSet();
            BufferedReader br = new BufferedReader(new InputStreamReader(ClavinLocationResolver.class.getClassLoader().getResourceAsStream("Demonyms.txt")));

            try {
                String line;
                while((line = br.readLine()) != null) {
                    demonyms.add(line);
                }

                br.close();
            } catch (IOException var4) {
                var4.printStackTrace();
            }
        }

        return demonyms.contains(extractedLocation.getRawValue());
    }
    public List<Location> resolveLocations(final List<Location> locations, final int maxHitDepth,
                                                   final int maxContextWindow, final boolean fuzzy) throws ClavinException {
        // are you forgetting something? -- short-circuit if no locations were provided
        if (locations == null || locations.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        // RB: turn off demonym filtering because we want those
        List<Location> filteredLocations;
        if (filterOutDemonyms) {
            /* Various named entity recognizers tend to mistakenly extract demonyms
             * (i.e., names for residents of localities (e.g., American, British))
             * as place names, which tends to gum up the works, so we make sure to
             * filter them out from the list of {@link LocationOccurrence}s passed
             * to the resolver.
             */
            filteredLocations = new ArrayList<Location>();
            for (Location location : locations)
                if (!isDemonym(location))
                    filteredLocations.add(location);
        } else {
            filteredLocations = locations;
        }
        // did we filter *everything* out?
        if (filteredLocations.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        QueryBuilder builder = new QueryBuilder()
                .maxResults(maxHitDepth)
                        // translate CLAVIN 1.x 'fuzzy' parameter into NO_EXACT or OFF; it isn't
                        // necessary, or desirable to support FILL for the CLAVIN resolution algorithm
                .fuzzyMode(fuzzy ? FuzzyMode.NO_EXACT : FuzzyMode.OFF)
                .includeHistorical(true);

        if (maxHitDepth > 1) { // perform context-based heuristic matching
            // stores all possible matches for each location name
            List<List<ResolvedLocation>> allCandidates = new ArrayList<List<ResolvedLocation>>();
            // loop through all the location names
            for (Location location : filteredLocations) {
                // get all possible matches
                List<ResolvedLocation> candidates = getGazetteer().getClosestLocations(builder.location(location.getRawValue()).build());

                // if we found some possible matches, save them
                if (candidates.size() > 0) {
                    allCandidates.add(candidates);
                }
            }
            // initialize return object
            //List<Location> bestCandidates = new ArrayList<Location>();

            List<ResolvedLocation> bestCandidates = disambiguationStrategy.select(this, allCandidates);
            for(ResolvedLocation rs: bestCandidates){
                System.out.println(rs.toString());
            }
            /*
            // split-up allCandidates into reasonably-sized chunks to
            // limit computational load when heuristically selecting
            // the best matches
            for (List<List<ResolvedLocation>> theseCandidates : ListUtils.chunkifyList(allCandidates, maxContextWindow)) {
                // select the best match for each location name based
                // based on heuristics
                bestCandidates.addAll(pickBestCandidates(theseCandidates));
            }
            */

            return null;
        } else { // use no heuristics, simply choose matching location with greatest population
            // initialize return object
            List<Location> resolvedLocations = new ArrayList<Location>();

            // stores possible matches for each location name
            List<Location> candidateLocations;

            // loop through all the location names
            for (Location location : filteredLocations) {
                // choose the top-sorted candidate for each individual
                // location name
               // candidateLocations = getGazetteer().getClosestLocations(builder.location(location.getRawValue()).build());

                // if a match was found, add it to the return list
//                if (candidateLocations.size() > 0) {
//                    resolvedLocations.add(candidateLocations.get(0));
//                }
            }

            return resolvedLocations;
        }
    }

}

