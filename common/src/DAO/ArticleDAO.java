package DAO;

import Entities.Article;

import java.sql.*;

/**
 * Created by Zane on 10/1/2015.
 */
public class ArticleDAO {
    Connection connection = null;

    public Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            if(connection == null)
                connection = DriverManager.getConnection("jdbc:mysql://localhost/ev_project?user=root&password=password");

        } catch (ClassNotFoundException e) {

            e.printStackTrace();

        } catch (SQLException e) {

            e.printStackTrace();

        }
        return connection;
    }

    public ArticleDAO() {
        getConnection();

    }

    public void insert(Article a){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO articles (id ,title,summary, body ,url, date, raw_date, hash,domain_id,location_string,tags) VALUES (NULL, ?, ?, ?,?, ?, ?,?,?,?,?)");
            preparedStatement.setString(1,a.getTitle());
            preparedStatement.setString(2, a.getSummary());
            preparedStatement.setString(3, a.getBody());
            preparedStatement.setString(4,a.getUrl());
            preparedStatement.setTimestamp(5, a.getDate());
            preparedStatement.setString(6, a.getRaw_date());
            preparedStatement.setString(7, a.getHash());
            preparedStatement.setInt(8, a.getDomain_id());
            preparedStatement.setString(9,a.getLoc());
            preparedStatement.setString(10,a.getTags());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public boolean titleExists(String hash){
        int count = 0;
        try {
            String queryCheck = "SELECT count(*) from articles WHERE title = ?";
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setString(1, hash);
            ResultSet resultSet = ps.executeQuery();
            if(resultSet.next()) {
                count = resultSet.getInt(1);
            }
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if(count > 0){
            return true;
        } else {
            return false;
        }
    }
}
