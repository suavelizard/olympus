package DAO;

import Entities.ArticleTemplate;

import java.sql.*;

/**
 * Created by Zane on 9/30/2015.
 */
public class ArticleTemplateDAO {
    Connection connection = null;

    public Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            if(connection == null)
                connection = DriverManager.getConnection("jdbc:mysql://localhost/ev_project?user=root&password=password");

        } catch (ClassNotFoundException e) {

            e.printStackTrace();

        } catch (SQLException e) {

            e.printStackTrace();

        }
        return connection;
    }

    public ArticleTemplateDAO() {
        getConnection();

    }

    public ArticleTemplate getArticleTemplate(String host) {
        PreparedStatement preparedStatement = null;
        ArticleTemplate template = new ArticleTemplate();
        String selectTableSQL = "SELECT * FROM article_templates "
                + " WHERE host = ? LIMIT 1";
        try {
            preparedStatement = connection.prepareStatement(selectTableSQL);
            preparedStatement.setString(1, host);
            // execute update SQL stetement
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                return null;
            }
            else{
                rs.first();
                template.setTitle_identifier(rs.getString("title_identifier"));
                template.setBody_identifier(rs.getString("body_identifier"));
                template.setDate_identifier(rs.getString("date_identifier"));
                template.setDate_format(rs.getString("date_format"));
                template.setDomain_id(rs.getInt("domain_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return template;
    }
}
