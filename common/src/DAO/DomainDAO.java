package DAO;

import Entities.Domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Zane on 10/8/2015.
 */
public class DomainDAO {
    Connection connection = null;

    public Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            if(connection == null)
                connection = DriverManager.getConnection("jdbc:mysql://localhost/ev_project?user=root&password=password");

        } catch (ClassNotFoundException e) {

            e.printStackTrace();

        } catch (SQLException e) {

            e.printStackTrace();

        }
        return connection;
    }

    public DomainDAO() {
        getConnection();

    }

    public List<Domain> list() {
        List<Domain> domains = new LinkedList<Domain>();
        try {
            java.sql.Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM ev_project.domains");

            Domain domain = null;
            while(resultSet.next()){
                domain = new Domain();
                domain.setId(Integer.parseInt(resultSet.getString("id")));
                domain.setUrl(resultSet.getString("url"));
                domain.setHash(resultSet.getString("hash"));

                domains.add(domain);
            }
            resultSet.close();
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return domains;
    }
    public List<Domain> listActive() {
        List<Domain> domains = new LinkedList<Domain>();
        try {
            java.sql.Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM ev_project.domains WHERE active = 1");

            Domain domain = null;
            while(resultSet.next()){
                domain = new Domain();
                domain.setId(Integer.parseInt(resultSet.getString("id")));
                domain.setUrl(resultSet.getString("url"));
                domain.setHash(resultSet.getString("hash"));

                domains.add(domain);
            }
            resultSet.close();
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return domains;
    }
}
