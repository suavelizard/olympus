package DAO;

import java.sql.*;

/**
 * Created by Zane on 10/5/2015.
 */
public class UrlDAO {
    Connection connection = null;

    public Connection getConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            if(connection == null)
                connection = DriverManager.getConnection("jdbc:mysql://localhost/ev_project?user=root&password=password");

        } catch (ClassNotFoundException e) {

            e.printStackTrace();

        } catch (SQLException e) {

            e.printStackTrace();

        }
        return connection;
    }

    public UrlDAO() {
        getConnection();

    }
    public void insert(String s){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO urls (id ,url, hash) VALUES (NULL, ?, ?)");
            preparedStatement.setString(1,s);
            preparedStatement.setString(2,s);

            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public boolean urlExists(String u){
        int count = 0;
        try {
            String queryCheck = "SELECT count(*) from urls WHERE url = ?";
            PreparedStatement ps = connection.prepareStatement(queryCheck);
            ps.setString(1, u);
            ResultSet resultSet = ps.executeQuery();
            if(resultSet.next()) {
                count = resultSet.getInt(1);
            }
            ps.close();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if(count > 0){
            return true;
        } else {
            return false;
        }
    }
}
