package Entities;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

/**
 * Created by Zane on 11/4/2015.
 */
public class EventTest {

    @Test
    public void testComputeEventSimilarityWithIdentical() throws Exception {
        Event a = new Event();
        Event b = new Event();
        a.setRawTitle("U.K. Suspends Flights From Sinai Airport, Saying �Explosive Device� May Have Downed Russian Jet");
        a.setPeople(new ArrayList<>(asList("John Doe", "mike", "Angela Merkle", "Steven", "Dr. Strange")));
        a.setLocations(new ArrayList<>(asList("U.K.", "Sinai", "Russia")));
        a.setOrganizations(new ArrayList<>(asList("Sinai Airport")));
        b = new Event(a);
        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println(a.computeEventSimilarity(b));
        assertTrue(a.computeEventSimilarity(b) == 100);
    }
    @Test
    public void testComputeEventSimilarityWithSimilarTitle() throws Exception {
        Event a = new Event();
        Event b = new Event();
        a.setRawTitle("U.K. Suspends Flights From Sinai Airport, Saying �Explosive Device� May Have Downed Russian Jet");
        a.setPeople(new ArrayList<>(asList("John Doe", "mike", "Angela Merkle", "Steven", "Dr. Strange")));
        a.setLocations(new ArrayList<>(asList("U.K.", "Sinai", "Russia")));
        a.setOrganizations(new ArrayList<>(asList("Sinai Airport")));
        b = new Event(a);
        b.setRawTitle("Russian plane crash: US intelligence suggests bomb was planted by Islamic State as Britain suspends Sharm el-Sheikh flights - latest news");
        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println(a.computeEventSimilarity(b));
        assertTrue(a.computeEventSimilarity(b) > 20);
    }
    @Test
    public void testComputeEventSimilarityWithDifferentTitle() throws Exception {
        Event a = new Event();
        Event b = new Event();
        a.setRawTitle("U.K. Suspends Flights From Sinai Airport, Saying �Explosive Device� May Have Downed Russian Jet");
        a.setPeople(new ArrayList<>(asList("John Doe", "mike", "Angela Merkle", "Steven", "Dr. Strange")));
        a.setLocations(new ArrayList<>(asList("U.K.", "Sinai", "Russia")));
        a.setOrganizations(new ArrayList<>(asList("Sinai Airport")));
        b = new Event(a);
        b.setRawTitle("G4S could run 999 controls of three major British police forces");
        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println(a.computeEventSimilarity(b));
        assertTrue(a.computeEventSimilarity(b) > 75);
    }
    @Test
    public void testComputeEventSimilarityWithCompletelyDifferentEvent() throws Exception {
        Event a = new Event();
        Event b = new Event();
        a.setRawTitle("U.K. Suspends Flights From Sinai Airport, Saying �Explosive Device� May Have Downed Russian Jet");
        a.setPeople(new ArrayList<>(asList("John Doe", "mike", "Angela Merkle", "Steven", "Dr. Strange")));
        a.setLocations(new ArrayList<>(asList("U.K.", "Sinai", "Russia")));
        a.setOrganizations(new ArrayList<>(asList("Sinai Airport")));
        b.setRawTitle("G4S could run 999 controls of three major British police forces");
        a.setPeople(new ArrayList<>(asList("Santa Clause", "Jim", "Obama", "Mike Tyson", "Fredric")));
        a.setLocations(new ArrayList<>(asList("Israel.", "Canada", "Vancouver")));
        a.setOrganizations(new ArrayList<>(asList("BC Government")));
        System.out.println(a.toString());
        System.out.println(b.toString());
        System.out.println(a.computeEventSimilarity(b));
        assertTrue(a.computeEventSimilarity(b) < 30);
    }
}