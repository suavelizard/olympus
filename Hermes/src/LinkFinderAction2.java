import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RecursiveAction;

/**
 * Created by Zane on 10/31/2015.
 */
public class LinkFinderAction2 extends RecursiveAction{

    private String url;
    private Hermes hermes;
    protected LinkedBlockingQueue articlesToParse;
    /**
     * Used for statistics
     */
    private static final long t0 = System.nanoTime();

    public LinkFinderAction2(String url, Hermes hermes, LinkedBlockingQueue articlesToParse) {
        this.url = url;
        this.hermes = hermes;
        this.articlesToParse = articlesToParse;
    }

    @Override
    public void compute() {
        if (!hermes.visited(url)) {
            hermes.addVisited(url);
            System.out.println(url);

            try {
                List<RecursiveAction> actions = new ArrayList<>();
                    URL uriLink = new URL(url);
                    Connection connection = Jsoup.connect(uriLink.toString());
                    Connection.Response response = connection.timeout(10000).userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0").execute();
                    int statusCode = response.statusCode();
                    Document doc = null;
                    if (statusCode == 200) {
                        doc = response.parse();
                    }
                    //extract links from document
                    Elements links = doc.select("a");
                    for (Element e : links) {
                        URL elem_link = hermes.parseLink(e, uriLink.getHost());

                        if (elem_link != null) {
                            if (!hermes.visited(elem_link.toString()) && !actions.contains(elem_link.toString())) {
                                //make sure that the anchors only link to the same host as the current link
                                if (elem_link.getHost().equals(uriLink.getHost())) {
                                    actions.add(new LinkFinderAction2(elem_link.toString(), hermes,articlesToParse));
                                }
                            }
                        }
                    }

                articlesToParse.put(doc);
                System.out.println("Hermes added: " + doc.location() + "size " + articlesToParse.size() + ", " + hermes.size());
                if (hermes.size() == 1500) {
                    System.out.println("Time for visit 1500 distinct links= " + (System.nanoTime() - t0));
                    //System.out.println(hermes.toString());
                    hermes.saveVisited();
                }
                //invoke recursively
                invokeAll(actions);
            } catch (Exception e) {
                //ignore 404, unknown protocol or other server errors
                e.printStackTrace();
            }
        }
    }
}
