import DAO.UrlDAO;
import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Zane on 10/1/2015.
 */
public class Hermes {
    private ForkJoinPool mainPool;
    private Collection<String> visitedLinks;
    private String url;
    private UrlDAO urlDao;
    final  LinkedBlockingQueue<Document> articlesToParse;

    public Hermes(String startingURL, int maxThreads, LinkedBlockingQueue<Document> articlesToParse) {
        this.url = startingURL;
        this.mainPool = new ForkJoinPool(maxThreads);
        this.urlDao = new UrlDAO();
        this.articlesToParse = articlesToParse;
        this.visitedLinks  = Collections.synchronizedSet(new HashSet<>());
    }

    public void process() {
        mainPool.invoke(new LinkFinderAction2(this.url, this, (LinkedBlockingQueue) articlesToParse));
    }

    public int size() {
        return visitedLinks.size();
    }

    public void addVisited(String s) {
        visitedLinks.add(s);
    }

    public boolean visited(String s) {
        //shortcut to avoid database call if already in the in memory list
        if(visitedLinks.contains(s)){
            return true;
        }
        // Check the database
        if(urlDao.urlExists(s)){
            return true;
        }
        return false;
    }
    public void addDocumentToQueue(Document doc){
        articlesToParse.add(doc);
        System.out.println("Hermes adding doc: " + doc.location());

    }
    public void saveVisited(){
        Iterator iterator = visitedLinks.iterator();
        int i = 0;
        while(iterator.hasNext() && i < 1500){
            urlDao.insert((String)iterator.next());
            iterator.remove();
        }
    }
    public boolean sameDomain(String s){
        return true;
    }

    /**
     * Attempt to retrieve URL from Element
     *
     * @param e JSoup element
     * @return URL
     * @throws MalformedURLException
     */
    public URL parseLink(Element e, String domain) throws MalformedURLException {
        //TODO handle other protocols for now just returns the host if it cannot figure out a url
        Element element = e;
        UrlValidator urlValidator = new UrlValidator();
        if (e.attr("href").startsWith("/")) {
            element.attr("href", domain + e.attr("href"));
        }
        if (e.attr("href").startsWith("www")) {
            element.attr("href", "http://" + e.attr("href"));
        }
            if (!urlValidator.isValid(element.attr("href"))) {
            return null;
        }
        URL linkUrl = new URL(element.attr("href"));
        return linkUrl;
    }
    @Override
    public String toString(){
        return visitedLinks.toString();
    }
}
