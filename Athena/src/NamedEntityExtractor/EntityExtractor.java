package NamedEntityExtractor;

import Entities.ExtractedEntities;
import Entities.Location;
import Entities.Organization;
import Entities.Person;
import Utilities.LocationResolver;
import com.bericotech.clavin.ClavinException;
import com.bericotech.clavin.gazetteer.query.Gazetteer;
import com.bericotech.clavin.gazetteer.query.LuceneGazetteer;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.stats.ClassicCounter;
import edu.stanford.nlp.stats.Counter;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.Triple;
//import edu.stanford.nlp.process;

import java.io.*;
import java.util.*;

/**
 * Created by Zane on 11/5/2015.
 */
public class EntityExtractor {
    private final Counter<String> dfCounter;
    private final StanfordCoreNLP pipeline;
    private AbstractSequenceClassifier<CoreMap> namedEntityRecognizer;
    private final int numDocuments;
    private final Gazetteer gazetteer;
    private final LocationResolver resolver;
//    private final
    public EntityExtractor() throws IOException, ClassNotFoundException, ClavinException {
        dfCounter =  loadDfCounter("df-counts.ser");
        String modPath = "classifiers/models/";
        Properties props = new Properties();
//        props.put("pos.model", modPath + "pos-tagger/english-left3words/english-left3words-distsim.tagger");
//        props.put("ner.model", modPath + "ner/english.all.3class.distsim.crf.ser.gz");
//        props.put("parse.model", modPath + "lexparser/englishPCFG.ser.gz");
        props.put("annotators", "tokenize, ssplit, pos");
        props.setProperty("ner.useSUTime", "0");
        props.setProperty("tokenize.language", "en");

//        props.put("sutime.binders","0");
//        props.put("sutime.rules", modPath + "sutime/defs.sutime.txt, " + modPath + "sutime/english.sutime.txt");
//        props.put("dcoref.demonym", modPath + "dcoref/demonyms.txt");
//        props.put("dcoref.states", modPath + "dcoref/state-abbreviations.txt");
//        props.put("dcoref.animate", modPath + "dcoref/animate.unigrams.txt");
//        props.put("dcoref.inanimate", modPath + "dcoref/inanimate.unigrams.txt");
//        props.put("dcoref.big.gender.number", modPath + "dcoref/gender.data.gz");
//        props.put("dcoref.countries", modPath + "dcoref/countries");
//        props.put("dcoref.states.provinces", modPath + "dcoref/statesandprovinces");
//        props.put("dcoref.singleton.model", modPath + "dcoref/singleton.predictor.ser");
        pipeline = new StanfordCoreNLP(props);
        File gazetteerDir = new File("IndexDirectory/");
        if( !gazetteerDir.exists() || !gazetteerDir.isDirectory() ){
            System.out.println("Error loading gazetter");
        } else{
            System.out.println("Loaded gazetter");

        }
        gazetteer = new LuceneGazetteer(new File("IndexDirectory/"));
        resolver = new LocationResolver(gazetteer);
        this.numDocuments = (int) dfCounter.getCount("__all__");
        String serializedClassifier = "edu/stanford/nlp/models/ner/english.conll.4class.distsim.crf.ser.gz";
//        pipeline.
//        AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifier(serializedClassifier);
        InputStream mpis = new FileInputStream("edu/stanford/nlp/models/ner/english.conll.4class.distsim.prop");
        Properties mp = new Properties();
        mp.load(mpis);
        namedEntityRecognizer = (AbstractSequenceClassifier<CoreMap>)CRFClassifier.getClassifier(serializedClassifier,mp);

    }

    public ArrayList<String> textToSentences(String text){
        DocumentPreprocessor dp = new DocumentPreprocessor(new StringReader(text));
        dp.setKeepEmptySentences(false);
        ArrayList<String> sentences = new ArrayList<String>();
        for (List<HasWord> sentence : dp) {
            String sentenceString = Sentence.listToString(sentence);
            sentences.add(sentenceString.toString());
            System.out.println(sentence);
            //sentences.add(sentence.toString());
        }
        return sentences;
    }

    public void getSentences(String text) throws IOException, ClassNotFoundException {
        // creates a StanfordCoreNLP object, with POS tagging, lemmatization, NER, parsing, and coreference resolution


// create an empty Annotation just with the given text
        Annotation document = new Annotation(text);

// run all Annotators on this text
        pipeline.annotate(document);

// these are all the sentences in this document
// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);

        //namedEntityRecognizer = (AbstractSequenceClassifier<CoreMap>)CRFClassifier.getClassifier(serializedClassifier,mp);

        // This gets out entities with character offsets
        int j = 0;
        int totalEntities = 0;
//        ArrayList<String> entities = new ArrayList<>();
        HashMap<String, Integer> entityMap = new HashMap<>();
        ExtractedEntities entities = new ExtractedEntities();

        for (CoreMap sentence : sentences) {
            j++;
            List<Triple<String, Integer, Integer>> triples = namedEntityRecognizer.classifyToCharacterOffsets(sentence.toString());
            for (Triple<String, Integer, Integer> trip : triples) {
                totalEntities++;
                String entity = sentence.toString().substring(trip.second(), trip.third).toLowerCase();
                int position = trip.second();
                switch(trip.first){
                    case "PERSON":
                        Person p = new Person(position,entity);
                        if(entities.getLocations().contains(p)){
                            entities.getLocations().get(entities.getLocations().indexOf(p)).incrementOccurences();
                        } else {
                            entities.addPerson(p);
                        }
                        break;
                    case "LOCATION":
                        entities.addLocation(new Location(position,entity));
                        break;
                    case "ORGANIZATION":
                        entities.addOrganization(new Organization(position,entity));
                        break;
                    case "MISC":    // if you're using the slower 4class model
                        System.out.println("MISC");
                        break;
                }
//                System.out.printf("%s over character offsets [%d, %d) in sentence %d.%n",
//                        trip.first(), trip.second(), trip.third, j);
                //System.out.println(trip.first() +": "+ sentence.toString().substring(trip.second(),trip.third).toLowerCase());
                if (!entityMap.containsKey(entity)) {
                    entityMap.put(entity, Integer.parseInt("1"));
                } else {
                    entityMap.put(entity, entityMap.get(entity) + 1);
                }
                //entities.add(sentence.toString().substring(trip.second(),trip.third).toLowerCase());
            }
        }

        System.out.println("Total Entities: " + entities.size());
        System.out.println(entityMap.toString());

    }
//    public ArrayList<String> removeDuplicates(ArrayList<String> list){
//            // Store unique items in result.
//            ArrayList<String> result = new ArrayList<>();
//
//            // Record encountered Strings in HashSet.
//            HashSet<String> set = new HashSet<>();
//
//            // Loop over argument list.
//            for (String item : list) {
//
//                // If String is not in set, add it to the list and the set.
//                if (!set.contains(item)) {
//                    result.add(item);
//                    set.add(item);
//                }
//            }
//            return result;
//        }
//    public static void main(String[] args){
//        EntityExtractor e = null;
//        try {
//            e = new EntityExtractor();
//            e.getSentences(rawText);
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        } catch (ClassNotFoundException e1) {
//            e1.printStackTrace();
//        } catch (ClavinException e1) {
//            e1.printStackTrace();
//        }
//
//    }

    public String summarize(List<CoreMap> sentences, int numSentences){

        Counter<String> tfs = getTermFrequencies(sentences);
        sentences = rankSentences(sentences, tfs);

        StringBuilder ret = new StringBuilder();
        for (int i = 0; i < numSentences; i++) {
            ret.append(sentences.get(i));
            ret.append("\n");
        }

       return ret.toString();


    }
    public void annotator(String text) throws IOException, ClassNotFoundException {
        String serializedClassifier = "classifiers/english.all.3class.distsim.crf.ser.gz";

        AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifier(serializedClassifier);
        String[] example = {"Good afternoon Rajat Raina, how are you today?",
                "I go to school at Stanford University, which is located in California." };
        for (String str : example) {
            System.out.println(classifier.classifyToString(str));
        }
        System.out.println("---");

        for (String str : example) {
            // This one puts in spaces and newlines between tokens, so just print not println.
            System.out.print(classifier.classifyToString(str, "slashTags", false));
        }
        System.out.println("---");

        for (String str : example) {
            // This one is best for dealing with the output as a TSV (tab-separated column) file.
            // The first column gives entities, the second their classes, and the third the remaining text in a document
            System.out.print(classifier.classifyToString(str, "tabbedEntities", false));
        }
        System.out.println("---");

        for (String str : example) {
            System.out.println(classifier.classifyWithInlineXML(str));
        }
        System.out.println("---");

        for (String str : example) {
            System.out.println(classifier.classifyToString(str, "xml", true));
        }
        System.out.println("---");

        for (String str : example) {
            System.out.print(classifier.classifyToString(str, "tsv", false));
        }
        System.out.println("---");

        // This gets out entities with character offsets
        int j = 0;
        for (String str : example) {
            j++;
            List<Triple<String,Integer,Integer>> triples = classifier.classifyToCharacterOffsets(str);
            for (Triple<String,Integer,Integer> trip : triples) {
                System.out.printf("%s over character offsets [%d, %d) in sentence %d.%n",
                        trip.first(), trip.second(), trip.third, j);
            }
        }
        System.out.println("---");

        // This prints out all the details of what is stored for each token
        int i=0;
        for (String str : example) {
            for (List<CoreLabel> lcl : classifier.classify(str)) {
                for (CoreLabel cl : lcl) {
                    System.out.print(i++ + ": ");
                    System.out.println(cl.toShorterString());
                }
            }
        }

        System.out.println("---");

    }
        private static Counter<String> getTermFrequencies(List<CoreMap> sentences) {
        Counter<String> ret = new ClassicCounter<String>();

        for (CoreMap sentence : sentences)
            for (CoreLabel cl : sentence.get(CoreAnnotations.TokensAnnotation.class))
                ret.incrementCount(cl.get(CoreAnnotations.TextAnnotation.class));

        return ret;
    }

    private class SentenceComparator implements Comparator<CoreMap> {
        private final Counter<String> termFrequencies;

        public SentenceComparator(Counter<String> termFrequencies) {
            this.termFrequencies = termFrequencies;
        }

        @Override
        public int compare(CoreMap o1, CoreMap o2) {
            return (int) Math.round(score(o2) - score(o1));
        }

        /**
         * Compute sentence score (higher is better).
         */
        private double score(CoreMap sentence) {
            double tfidf = tfIDFWeights(sentence);

            // Weight by position of sentence in document
            int index = sentence.get(CoreAnnotations.SentenceIndexAnnotation.class);
            double indexWeight = 5.0 / index;

            return indexWeight * tfidf * 100;
        }

        private double tfIDFWeights(CoreMap sentence) {
            double total = 0;
            for (CoreLabel cl : sentence.get(CoreAnnotations.TokensAnnotation.class))
                if (cl.get(CoreAnnotations.PartOfSpeechAnnotation.class).startsWith("n"))
                    total += tfIDFWeight(cl.get(CoreAnnotations.TextAnnotation.class));

            return total;
        }

        private double tfIDFWeight(String word) {
            if (dfCounter.getCount(word) == 0)
                return 0;

            double tf = 1 + Math.log(termFrequencies.getCount(word));
            double idf = Math.log(numDocuments / (1 + dfCounter.getCount(word)));
            return tf * idf;
        }
    }

    private List<CoreMap> rankSentences(List<CoreMap> sentences, Counter<String> tfs) {
        Collections.sort(sentences, new SentenceComparator(tfs));
        return sentences;
    }
    @SuppressWarnings("unchecked")
    private static Counter<String> loadDfCounter(String path)
            throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
        return (Counter<String>) ois.readObject();
    }

//    public ExtractedEntities extractEntities(String textToParse, boolean manuallyReplaceDemonyms) {
//        ExtractedEntities entities = new ExtractedEntities();
//
//        if (textToParse==null || textToParse.length()==0){
//            logger.warn("input to extractEntities was null or zero!");
//            return entities;
//        }
//
//        String text = textToParse;
//
//
//        // extract entities as <Entity Type, Start Index, Stop Index>
//        List<Triple<String, Integer, Integer>> extractedEntities =
//                namedEntityRecognizer.classifyToCharacterOffsets(text);
//
//        if (extractedEntities != null) {
//            for (Triple<String, Integer, Integer> extractedEntity : extractedEntities) {
//                String entityName = text.substring(extractedEntity.second(), extractedEntity.third());
//                int position = extractedEntity.second();
//
//            }
//        }
//
//        return entities;
//    }
    private static String rawText = "Vancouver City Council has unanimously passed a number of recommendations to modernize the Vancouver taxi and \"vehicle for hire\" industry, with suggestions most notably regarding the ride sharing service Uber. "+
            "Council met Thursday evening to discuss the proposed changes outlined in an October 13 report, with a fair amount of time dedicated to discussing Uber's potential entry into the Vancouver market. "+
            "Michael van Hemmen, public policy manager at Uber Canada in Vancouver - a ridesharing service which uses a smartphone application to connect riders with available drivers to provide taxi services using their personal vehicles - presented his company's proposal for doing business in Vancouver, which along with Metro Vancouver as a whole is the largest region in North America without a ridesharing service.\n"+
            "The report issued in October identified both the benefits and concerns regarding allowing Uber to enter Vancouver.\n"+
            "\"In markets that Uber is operational, there is evidence that the significant increase in supply has resulted in shorter wait times, lower fares, and higher customer satisfaction,\" the document reads. \"On the other hand, rideshare raises significant concerns about passenger safety, future taxi industry viability, and the availability of accessible service to disabled persons.\"\n"+
            "The October report called for staff to report back to council after laising with other local governments, the Passenger Transportation Board (PTB), ICBC, the taxi and ridesharing industries and stakeholder groups and examining \"the issues and opportunities for rideshare in Metro Vancouer\". A subsequent City presentation recommended council vote to examine these issues along with additional consideration.\n"+
            "According to van Hemmen, a recent poll found that 70% of Metro Vancouver residents support ridesharing in the province, not including the many tourists who enter Vancouver looking for Uber service like they are familiar with at home.\n"+
            "Despite the company's popularity among consumers, the provincial and municipal government has been apprehensive to give Uber the go-ahead to operate in Vancouver. Issues unique to B.C. like insurance, licensing and the influence of the taxi industry lobby have each been massive obstacles in Uber's way.\n"+
            "During the meeting, Councillor Adrienne Carr raised a question to Uber about whether or not B.C. drivers with a Class 5 license would legally be allowed to function as vehicles for hire and earn compensation for the driver. Under current legislation, Uber would need the provincial government to update these rules for the system to work.\n"+
            "Another obstacle is the issue of insurance with ICBC, as brought up in the meeting by Mayor Gregor Robertson. Van Hemmen says Uber's insurance policy would need to work in partnership with ICBC, which may be more complicated than with the private insurance companies Uber deals with across the country.\n"+
            "Even with the regulatory roadblocks, Councillor Geoff Meggs believes there is a real threat Uber may begin operating without first having the green light from all governments and stakeholders.\n"+
            "The company previously launched here in the summer of 2012, when the concept's impact was not yet so internationally known. Upon growing more popular throughout that year, the Passenger Transportation Board slapped Uber with a reminder that the minimum charge for limousine services was $75 per trip. Uber's framework fell well below that price point.\n"+
            "Following the failure of their first launch, Uber again expressed their intention to launch in 2014 and was then faced with a lawsuit filed by Black Top, Yellow Cab and Vancouver Taxi. Since Uber never successfully launched, the taxi companies dropped their suit in March, but indicated they would refile if Uber relaunched with a service that violated the legal requirements for taxi service.\n"+
            "During the meeting, Meggs asked Uber if they would promise not to launch before the legal and regulatory framework of the province is met.\n"+
            "\"Will you commit tonight that you will not launch until you have appropriate policy direction from the provincial government?\" asked Meggs.\n"+
            "\"Absolutely, our intent is that we follow laws,\" van Hemmen responded.\n"+
            "\"Good to know.\"\n"+
            "\"As I've explained here is if there is a space in which we fall in which we are not captured [by the regulatory framework] then absolutely, then we are not captured by the existing regulatory framework. I'm trying to be as clear with you as possible.\"\n"+
            "\"You're not succeeding completely... What I've heard you say is that you will not be launching until Uber has a legal way to do so?\" Meggs asked.\n"+
            "\"Well, I would say that we don't have a date set for launching at this time,\" van Hemmen answered to an audible outcry from councillors and audience.\n"+
            "Van Hemmen stated that Uber would welcome more engagement from the province to create a framework that embraces the ridesharing and Uber concept, however, when asked again to promise not to launch until the framework is completed, he did not answer the question, instead stating that they were continuing to work collaboratively with both the city and province and hope to do so in the future.\n"+
            "Uber was also met with a hostile interaction with Councillor Deal who hard-lined the company's training process, particularly for customers with disabilities.\n"+
            "\"I don't think it's our job here at Council to grease the skids for a large corporation that wants to come in here and skim off a large amount of money.. I don't see them as an economic benefit,\" Meggs deb'ted at the end of the meeting.\n"+
            "Uber has sparked controversy around the world for working around local laws and bringing unfair competition to established taxi businesses. For instance, new data in New York City indicates there are now 14,088 Uber affiliated vehicles within the city's five boroughs compared to 13,587 taxis.\n"+
            "But its popularity cannot be denied: Uber now operates in 61 countries and 340 cities around the world, including Toronto, Montreal, Edmonton, Calgary and Halifax. The company says in the last month alone, over 25,000 people opened the Uber app in British Columbia looking for a ride.";
}
//        ArrayList<String> deduplicated = removeDuplicates(entities);
//        System.out.println("After de-duplciation: " + deduplicated.size());
//
//        for (String s: deduplicated){
//            System.out.println(s);
//        }
//        for(CoreMap sentence: sentences) {
//            System.out.println(sentence.toString());
//            // traversing the words in the current sentence
//            // a CoreLabel is a CoreMap with additional token-specific methods
//            for (CoreLabel token: sentence.get(CoreAnnotations.TokensAnnotation.class)) {
//                // this is the text of the token
//                String word = token.get(CoreAnnotations.TextAnnotation.class);
//                // this is the POS tag of the token
//                String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
////                System.out.println(pos+": "+ word);
//                // this is the NER label of the token
//                String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
//                System.out.println(word + ": " + ne);
//            }
//
//        }
//}