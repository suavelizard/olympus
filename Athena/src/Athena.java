import DAO.ArticleDAO;
import DAO.ArticleTemplateDAO;
import Entities.Article;
import Entities.ArticleTemplate;
import NamedEntityExtractor.EntityExtractor;
import Utilities.DateParser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Zane on 10/31/2015.
 */
public class Athena{
    private final LinkedBlockingQueue<Document> bq;
    private ArticleDAO articleDao;
    private final EntityExtractor extractor;
    public Athena(LinkedBlockingQueue<Document> bq, EntityExtractor extractor){
        this.articleDao = new ArticleDAO();
        this.bq = bq;
        this.extractor = extractor;


    }

    public void consume(){
        try {
            Document d = (Document) bq.take();
            URL u = new URL(d.location());
            ArticleTemplateDAO articleTemplateDao = new ArticleTemplateDAO();
            ArticleTemplate articleTemplate = articleTemplateDao.getArticleTemplate(u.getHost());
            Element title = d.select(articleTemplate.getTitle_identifier()).first();
            Elements body = d.select(articleTemplate.getBody_identifier());
            Element date = d.select(articleTemplate.getDate_identifier()).first();

            Timestamp t = null;
            Article a = new Article();

            //When? Figure out the time.
            if (date != null && convertDateToTimestamp(date.text()) != null) {
                t = convertDateToTimestamp(date.text());
                a.setDate(t);
                //set raw date for reference
                a.setRaw_date(date.text());
            }

//            Where? Figure out the geo location
            extractor.getSentences(body.text());
//            What? Figure out the tags and summary
            if (body != null) {
                a.setBody(body.text());
            }
            if (title != null) {
                a.setTitle(title.text());
            }
            if (a.isValid()) {
                //Save to DB
                //Check for duplicate
                System.out.println("Athena learned about: " + a.getTitle());
            } else {
                System.out.println("Athena ignored: "+ d.location()+" "+u.getHost());
            }
      } catch (MalformedURLException mue){
            mue.printStackTrace();
        }
        catch (InterruptedException ie){
            ie.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }

    public Timestamp convertDateToTimestamp(String s){
        Timestamp t = null;
        try {
            if(s!=null) {
                DateParser parser = new DateParser(s);
                t = parser.getTimestamp();
            }
        } catch (ParseException pe){
            pe.printStackTrace();
        }
        return t;
    }

    public void compareEvents(){

    }
//
//    public static HashMap parseFromEntities(ExtractedEntities entities){
//        if (entities == null){
//            return getErrorText("No place or person entitites detected in this text.");
//        }
//
//        logger.debug("Adding Mentions:");
//        HashMap results = new HashMap();
//        // assemble the "where" results
//        HashMap placeResults = new HashMap();
//        ArrayList resolvedPlaces = new ArrayList();
//        for (ResolvedLocation resolvedLocation: entities.getResolvedLocations()){
//            HashMap loc = writeResolvedLocationToHash(resolvedLocation);
//            resolvedPlaces.add(loc);
//        }
//        placeResults.put("mentions",resolvedPlaces);
//
//        logger.debug("Adding Focus:");
//        HashMap focusResults = new HashMap();
//        if (resolvedPlaces.size() > 0){
//            ArrayList focusLocationInfoList;
//            logger.debug("Adding Country Focus:");
//            focusLocationInfoList = new ArrayList<HashMap>();
//            for(FocusLocation loc:focusStrategy.selectCountries(entities.getResolvedLocations())) {
//                focusLocationInfoList.add( writeAboutnessLocationToHash(loc) );
//            }
//            focusResults.put("countries", focusLocationInfoList);
//            logger.debug("Adding State Focus:");
//            focusLocationInfoList = new ArrayList<HashMap>();
//            for(FocusLocation loc:focusStrategy.selectStates(entities.getResolvedLocations())) {
//                focusLocationInfoList.add( writeAboutnessLocationToHash(loc) );
//            }
//            focusResults.put("states", focusLocationInfoList);
//            logger.debug("Adding City Focus:");
//            focusLocationInfoList = new ArrayList<HashMap>();
//            for(FocusLocation loc:focusStrategy.selectCities(entities.getResolvedLocations())) {
//                focusLocationInfoList.add( writeAboutnessLocationToHash(loc) );
//            }
//            focusResults.put("cities", focusLocationInfoList);
//        }
//        placeResults.put("focus",focusResults);
//        results.put("places",placeResults);
//
//        logger.debug("Adding People:");
//        // assemble the "who" results
//        List<ResolvedPerson> resolvedPeople = entities.getResolvedPeople();
//        List<HashMap> personResults = new ArrayList<HashMap>();
//        for (ResolvedPerson person: resolvedPeople){
//            HashMap sourceInfo = new HashMap();
//            sourceInfo.put("name", person.getName());
//            sourceInfo.put("count", person.getOccurenceCount());
//            personResults.add(sourceInfo);
//        }
//        results.put("people",personResults);
//
//        logger.debug("Adding Organizations:");
//        // assemble the org results
//        List<ResolvedOrganization> resolvedOrganizations = entities.getResolvedOrganizations();
//        List<HashMap> organizationResults = new ArrayList<HashMap>();
//        for (ResolvedOrganization organization: resolvedOrganizations){
//            HashMap sourceInfo = new HashMap();
//            sourceInfo.put("name", organization.getName());
//            sourceInfo.put("count", organization.getOccurenceCount());
//            organizationResults.add(sourceInfo);
//        }
//        results.put("organizations",organizationResults);
//
//        HashMap response = getResponseMap( results );
//        return response;
//    }
}
