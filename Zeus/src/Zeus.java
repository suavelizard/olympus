import DAO.DomainDAO;
import Entities.Domain;
import NamedEntityExtractor.EntityExtractor;
import com.bericotech.clavin.ClavinException;
import org.jsoup.nodes.Document;

import javax.swing.text.html.parser.Entity;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Zane on 10/31/2015.
 */
public class Zeus {
    private final EntityExtractor extractor;
    public Zeus(int numHermes, int numAthena) throws IOException, ClavinException, ClassNotFoundException {
        //Create Extractor
        extractor = new EntityExtractor();
        //Create queue for Documents needing to be processed
        LinkedBlockingQueue<Document> queue = new LinkedBlockingQueue<>(100);

        //Create executor services for threading
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        //Create database access object for domains
        DomainDAO domainDao = new DomainDAO();

        //Get all top level domains listed as active
        List<Domain> domains = domainDao.listActive();

        //Create Single Hermes with numHermes sub-threads to crawl and scrape URLS
        final Hermes producer = new Hermes("http://www.wsj.com",4, queue);
        executorService.submit(() -> {
            while (true) {
                producer.process();
            }
        });
        System.out.println("hermes");
        //Create Athenas to process Documents
        for (int i = 0; i < 2; i++) {
            final Athena consumer = new Athena(queue, extractor);
            executorService.submit(() -> {
                while (true) {
                    consumer.consume();
                }
            });
        }

        executorService.shutdown();
        try {
            executorService.awaitTermination(10, TimeUnit.SECONDS);
            executorService.shutdownNow();
        } catch (InterruptedException e) {
            System.out.println("Error waiting for ExecutorService shutdown");
        }
    }
}

//
//        System.out.println("Hermes being sent out");
//
//        // Start Hermes getting URLs
//        for(Hermes h: hermesList){
//            h.start();
//        }
//        //Get Athena ready to process
//        System.out.println("Sending Athena to figure stuff out");
//        try{
//            ExecutorService executor = Executors.newFixedThreadPool(numAthena);
//            executor.execute(new Athena(articlesToParse));
//        } catch(MalformedURLException mue){
//            mue.printStackTrace();
//        } catch (InterruptedException ie){
//            ie.printStackTrace();
//        }
//
//    }
//


//}

