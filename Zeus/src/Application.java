import com.bericotech.clavin.ClavinException;

import java.io.IOException;

/**
 * Created by Zane on 10/31/2015.
 */

public class Application {
    public static final int NUMBER_OF_HERMES_THREADS = 2;
    public static final int NUMBER_OF_ATHENA_THREADS = 4;

    public static void main(String[] args) throws InterruptedException {
        try {
            new Zeus(NUMBER_OF_HERMES_THREADS, NUMBER_OF_ATHENA_THREADS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClavinException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}